﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Support.UI;
using System.Diagnostics;
using System.IO;
//using System.Configuration;
//using System.Data.SqlClient;

namespace DemoAutoTests.Hooks
{
    public static class StepsContext
    {
        public static IWebDriver Driver;
        public static WebDriverWait Wait;
    }

    [Binding]
    public static class Hooks
    {
        [BeforeTestRun]
        public static void RunChromeBrower()
        {
            StepsContext.Driver = new ChromeDriver(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
            StepsContext.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            StepsContext.Driver.Manage().Window.Maximize();
            StepsContext.Wait = new WebDriverWait(StepsContext.Driver, TimeSpan.FromSeconds(10));

            //var ConnectionString = ConfigurationManager.ConnectionStrings["InsappTest"].ConnectionString;
            //using (var connection = new SqlConnection(ConnectionString))
            //{
            //    var command = new SqlCommand("SELECT * FROM InternalUsers", connection);
            //    using (var reader = command.ExecuteReader())
            //    {
            //        while (reader.Read())
            //        {
            //            Console.WriteLine(String.Format("{0}, {1}", reader[0], reader[1]));
            //        }
            //    }
            //}
        }

        [BeforeTestRun]
        public static void DeleteOldErrorScreenshots()
        {
            DirectoryInfo DirInfo = new DirectoryInfo($"{Path.GetDirectoryName(Directory.GetCurrentDirectory())}\\ErrorScreenshots");
            foreach (DirectoryInfo SubDir in DirInfo.GetDirectories())
                SubDir.Delete(true);
            foreach (FileInfo File in DirInfo.GetFiles())
                File.Delete();
        }

        [AfterTestRun]
        public static void CloseBrower()
        {
            StepsContext.Driver.Close();
            StepsContext.Driver.Dispose();
            using (Process LivingDocCreate = new Process())
            {
                string Path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                LivingDocCreate.StartInfo.FileName = "cmd.exe";
                LivingDocCreate.StartInfo.UseShellExecute = false;
                LivingDocCreate.StartInfo.CreateNoWindow = true;
                LivingDocCreate.StartInfo.Arguments = $"/c cd {Path} && livingdoc test-assembly {Path}\\DemoAutoTests.dll -t {Path}\\TestExecution.json";
                LivingDocCreate.Start();
            }
        }
    }
}
