﻿Feature: DriversDataPage
![Logo](https://www.insapp.ru/local/templates/insapp/img/icons/logo.svg)
UI tests for drivers data page.

Link to a feature: [DriversDataPage](DemoAutoTests/Features/DriversDataPage.feature)
**[Documentation for this page](https://projects.myalm.ru/pm/InsappProductManagement/R-650)**

@mytag
Scenario Outline: Check errors message
	Given I open KASKO widget
	And I go on DriversData page
	When I enter <Value> in <Field>
	And I enter <SecondValue> in Age
	When I press NextButton button
	Then Attribute outerText in <ErrorField> should be <ExpectedValue>

Examples: 
| N                            | Field            | Value | SecondValue | ErrorField            | ExpectedValue                        |
| Empty age                    | Age              |       |             | AgeError              | Укажите, сколько полных лет водителю |
| Age > 18                     | Age              | 17    | 17          | AgeError              | Не менее 18 лет                      |
| Empty driver experience      | DriverExperience |       |             | DriverExperienceError | Укажите стаж водителя                |
| Driver experience > age - 18 | DriverExperience | 1     | 18          | DriverExperienceError | Укажите корректный стаж водителя     |
| Empty gender                 | Gender           |       |             | GenderError           | Укажите пол водителя                 |
| Empty marital status         | MaritalStatus    |       |             | MaritalStatusError    | Укажите семейный статус водителя     |
| Empty child count			   | ChildCount		  |       |             | ChildCountError	    | Укажите количество детей у водителя  |


Scenario Outline: Check validation
	Given I open KASKO widget
	And I go on DriversData page
	When I enter <Value> in <Field>
	And I press NextButton button
	Then Attribute <Attribute> in <Field> should be <ExpectedValue>

Examples: 
| N                                        | Field            | Value          | Attribute | ExpectedValue							   |
| Invalid symbols in age                   | Age              | fFпП,./*+-=?!~ | value     |										   |
| More then 2 symbols in age               | Age              | 999            | value     | 99										   |
| Invalid symbols in driver experience     | DriverExperience | fFпП,./*+-=?!~ | value     |										   |
| More then 2 symbols in driver experience | DriverExperience | 999            | value     | 99										   |
| Zero in driver experience				   | DriverExperience | 0              | class     | input__field ng-touched ng-dirty ng-valid |


Scenario: Check "Add driver" button: Empty fields
	Given I open KASKO widget
	And I go on DriversData page
	When I press AddDriverButton button
	Then Attribute outerText in AgeError should be Укажите, сколько полных лет водителю
	And Attribute outerText in DriverExperienceError should be Укажите стаж водителя
	And Attribute outerText in GenderError should be Укажите пол водителя
	And Attribute outerText in MaritalStatusError should be Укажите семейный статус водителя
	And Attribute outerText in ChildCountError should be Укажите количество детей у водителя
	And Element DriversCount should have 1 child with li name


Scenario: Check "Add driver" button: Add new driver
	Given I open KASKO widget
	And I go on DriversData page
	When I enter 69 in Age
	And I enter 21 in DriverExperience
	And I click on MaleGender
	And I click on Married
	And I click on TwoChild
	And I press AddDriverButton button
	Then Element DriversCount should have 2 child with li name


Scenario: Check "Back/Delete" button: Change button name
	Given I open KASKO widget
	And I go on DriversData page
	When I enter 69 in Age
	And I enter 21 in DriverExperience
	And I click on MaleGender
	And I click on Married
	And I click on TwoChild
	And I press AddDriverButton button
	Then Attribute outerText in BackButton should be УДАЛИТЬ ВОДИТЕЛЯ


Scenario: Check "Back/Delete" button: Delete driver
	Given I open KASKO widget
	And I go on DriversData page
	When I enter 69 in Age
	And I enter 21 in DriverExperience
	And I click on MaleGender
	And I click on Married
	And I click on TwoChild
	And I press AddDriverButton button
	And I press BackButton button
	Then Element DriversCount should have 1 child with li name
	And Attribute outerText in BackButton should be НАЗАД
	And Attribute value in Age should be 69
	And Attribute value in DriverExperience should be 21
	And Attribute value in Gender should be 1
	And Attribute value in MaritalStatus should be 2
	And Attribute value in ChildCount should be 2