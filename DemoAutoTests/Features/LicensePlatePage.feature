﻿Feature: LicensePlatePage
![Logo](https://www.insapp.ru/local/templates/insapp/img/icons/logo.svg)
UI tests for license plate page.

Link to a feature: [LicensePlatePage](DemoAutoTests/Features/LicensePlatePage.feature)
**[Documentation for this page](https://projects.myalm.ru/pm/InsappProductManagement/R-1435)**

@mytag
Scenario Outline: Check errors message
	Given I open KASKO widget
	And I go on LicensePlate page
	When I enter <Value> in Number
	And I enter <SecondValue> in Region
	And I press NextButton button
	Then Attribute outerText in LicensePlateError should be Укажите номер автомобиля

Examples: 
| N                | Value  | SecondField |
| Empty fields     |        |             | 
| Empty number     |        | 55          |
| Empty region     | Т581СВ |             |
| Incorrect number | Т581   | 55          |
| Incorrect region | Т581СВ | 5           |


Scenario Outline: Check validation
	Given I open KASKO widget
	And I go on LicensePlate page
	When I enter <Value> in <Field>
	Then Attribute value in <Field> should be <ExpectedValue>

Examples: 
| N                                              | Value                         | Field  | ExpectedValue |
| Incorrect russian letters in number            | бгдёжзийлпфцчшщъэьюя          | Number |               |
| Incorrect english letters in number            | abgiklmopqsuwxz               | Number |               |
| Mask in number (Used Unicode code instead "У") | U+043Ee1234U+043EU+043EU+043E | Number | У123УУ        |
| Invalid symbols in region                      | dDпП()-+*/=.,~%&!             | Region |               |
| Transform english letter: F -> A               | f                             | Number | А             |
| Transform english letter: D -> В               | d                             | Number | В             |
| Transform english letter: T -> Е               | t                             | Number | Е             |
| Transform english letter: V -> м               | v                             | Number | М             |
| Transform english letter: Y -> Н               | y                             | Number | Н             |
| Transform english letter: J -> О               | j                             | Number | О             |
| Transform english letter: H -> Р               | h                             | Number | Р             |
| Transform english letter: C -> C               | c                             | Number | С             |
| Transform english letter: N -> Т               | n                             | Number | Т             |
| Transform english letter: E -> У               | e                             | Number | У             |
| Transform symbol: { -> Х                       | {                             | Number | Х             |
| Transform symbol: [ -> Х                       | [                             | Number | Х             |
