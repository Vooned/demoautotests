﻿Feature: OwnerDataPage
![Logo](https://www.insapp.ru/local/templates/insapp/img/icons/logo.svg)
UI tests for owner data page.

Link to a feature: [OwnerDataPage](DemoAutoTests/Features/OwnerDataPage.feature)
**[Documentation for this page](https://projects.myalm.ru/pm/InsappProductManagement/R-681)**

@mytag
Scenario Outline: Check error messages
	Given I open KASKO widget
	And I go on OwnerData page
	When I enter <Value> in <Field>
	And I press NextButton button
	Then Attribute outerText in <ErrorField> should be <ExpectedValue>

Examples: 
| N                | Field     | Value                          | ErrorField     | ExpectedValue                          |
| Empty adress     | Adress    |                                | AdressError    | Укажите город регистрации собственника |
| Incorrect adress | Adress    | Улица Пушкина, дом Колотушкина | AdressError    | Выберите город из выпадающего списка   |
| Empty owner name | OwnerName |                                | OwnerNameError | Укажите имя собственника               |
| Incorrect phone  | Phone     | 12345                          | PhoneError     | Некорректный номер телефона            |


Scenario: Check error messages: Empty phone
	Given I open KASKO widget
	And I go on OwnerData page
	When I press NextButton button
	Then Attribute outerText in PhoneError should be Укажите телефон собственника


Scenario: Check error message in sms window
	Given I open KASKO widget
	And I go on OwnerData page
	When I enter Москва in Adress
	And I wait 0,1 seconds
	And I press "Down" key in Adress
	And I wait 0,1 seconds
	And I press "Enter" key in Adress
	And I enter Тестовый Витюша in OwnerName
	And I enter 9605461075 in Phone
	When I press NextButton button
	And I enter 0 in FirstSmsInput
	And I enter 0 in SecondSmsInput
	And I enter 0 in ThirdSmsInput
	And I enter 0 in FourthSmsInput
	And I wait 0,01 seconds
	Then Attribute outerText in ModalTitle should be Введён неверный код


Scenario Outline: Check validation
	Given I open KASKO widget
	And I go on OwnerData page
	When I enter <Value> in <Field>
	Then Attribute value in <Field> should be <ExpectedValue>

Examples: 
| N                             | Value            | Field     | ExpectedValue     |
| Invalid symbols in owner name | 0123456789~=*/,  | OwnerName |                   |
| Valid symbols inowner name    | GgПп -.          | OwnerName | GgПп -.           |
| Invalid symbols in phone      | GgПп.,-+/*=~()!? | Phone     | +7(___) ___-__-__ |