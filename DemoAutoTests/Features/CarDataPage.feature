﻿Feature: CarData Validation
![Logo](https://www.insapp.ru/local/templates/insapp/img/icons/logo.svg)
UI tests for car data page.

Link to a feature: [CarDataPage](DemoAutoTests/Features/CarDataPage.feature)
**[Documentation for this page](https://projects.myalm.ru/pm/InsappProductManagement/R-232)**


Scenario: Check brand validation
	Given I open KASKO widget
	And I go on CarData page
	When I press NextButton button
	Then Attribute outerText in BrandError should be Укажите марку автомобиля

Scenario: Check model validation
	Given I open KASKO widget
	And I go on CarData page
	When I enter Nissan in Brand
	And I press "Enter" key in Brand
	When I enter Невалидная модель in Model
	When I press NextButton button
	Then Attribute outerText in ModelError should be Укажите модель автомобиля