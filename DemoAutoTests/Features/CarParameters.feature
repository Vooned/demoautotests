﻿Feature: CarParameters
![Logo](https://www.insapp.ru/local/templates/insapp/img/icons/logo.svg)
UI tests for car parameters page.

Link to a feature: [CarParametersPage](DemoAutoTests/Features/CarParametersPage.feature)
**[Documentation for this page](https://projects.myalm.ru/pm/InsappProductManagement/R-632)**

Scenario Outline: Check errors message
	Given I open KASKO widget
	And I go on CarParameters page
	When I clear <Field>
	And I enter <Value> in <Field>
	And I press NextButton button
	Then Attribute outerText in <CheckedField> should be <ExpectedValue>

Examples: 
| N                              | Value | Field          | CheckedField        | ExpectedValue                             |
| Empty milleage                 |       | Mileage        | MileageError        | Укажите пробег Вашего автомобиля          |
| Empty car power                |       | CarPower       | CarPowerError       | Укажите мощность Вашего автомобиля в л.с. |
| Car power > 30                 | 29    | CarPower       | CarPowerError       | Не менее 30 л.с.                          |
| Empty production year          |       | ProductionYear | ProductionYearError | Укажите год выпуска Вашего автомобиля     |
| Production year > 2007         | 2006  | ProductionYear | ProductionYearError | Не ранее 2007 года выпуска                |
| Future date in production year | 2022  | ProductionYear | ProductionYearError | Укажите корректный год выпуска            |
| Empty price                    |       | Price          | PriceError          | Укажите стоимость Вашего автомобиля       |

Scenario Outline: Check validation
	Given I open KASKO widget
	And I go on CarParameters page
	When I clear <Field>
	And I enter <Value> in <Field>
	And I press NextButton button
	Then Attribute <Attribute> in <CheckedField> should be <ExpectedValue>

Examples: 
| N									 | Value          | Field		   | CheckedField   | Attribute | ExpectedValue								|
| Invalid symbols in milleage		 | fFпП,./*+-=?!~ | Mileage		   | Mileage        | value     |											|
| Too big value in milleage			 | 12345678       | Mileage		   | Mileage        | value     | 1 234 567 км.								|
| Zero in milleage					 | 0              | Mileage		   | Mileage        | class     | input__field ng-touched ng-dirty ng-valid |
| Invalid symbols in car power       | fFпП,./*+-=?!~ | CarPower       | CarPower       | value     |											|
| Too big value in car power         | 12345          | CarPower       | CarPower       | value     | 1 234 л.с.								|
| Invalid symbols in production year | fFпП,./*+-=?!~ | ProductionYear | ProductionYear | value     |											|
| Too big value in production year   | 20211          | ProductionYear | ProductionYear | value     | 2021 год									|
| Invalid symbols in price           | fFпП,./*+-=?!~ | Price          | Price          | value     |											|
| Too big value in price             | 123456789      | Price          | Price          | value     | 12 345 678 руб.							|

