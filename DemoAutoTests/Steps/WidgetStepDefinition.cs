﻿using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using DemoAutoTests.Hooks;
using OpenQA.Selenium;
using FluentAssertions;
using DemoAutoTests.Elements;
using System.IO;

namespace DemoAutoTests.Steps
{
    [Binding]
    public sealed class WidgetStepDefinition
    {
        private FeatureContext _featureContent;
        private ScenarioContext _scenarioContext;
        public WidgetElement Element;

        public WidgetStepDefinition(FeatureContext featureContent, ScenarioContext scenarioContext)
        {
            _featureContent = featureContent;
            _scenarioContext = scenarioContext;
        }

        /// <summary>
        /// Создание скриншота и сохранение его в папке ErrorScreenshots
        /// </summary>
        public void TakeScreenShot()
        {
            DirectoryInfo MainCatalog = new DirectoryInfo($"{System.IO.Path.GetDirectoryName(Directory.GetCurrentDirectory())}\\ErrorScreenshots");
            DirectoryInfo SubCatalog = new DirectoryInfo($"{System.IO.Path.GetDirectoryName(Directory.GetCurrentDirectory())}\\ErrorScreenshots\\{_featureContent.FeatureInfo.Title}\\{_scenarioContext.ScenarioInfo.Title}");
            if (!SubCatalog.Exists)
                MainCatalog.CreateSubdirectory($"{_featureContent.FeatureInfo.Title}\\{_scenarioContext.ScenarioInfo.Title}");
            Screenshot ErrorScreen = ((ITakesScreenshot)StepsContext.Driver).GetScreenshot();
            string ScreenName = $"{_scenarioContext.ScenarioInfo.Arguments[0]} at {DateTime.Now:dd-MM-yyyy HH_mm_ss}";
            char[] SpecialCharacters = new char[] {'/','\\','*','?', '<', '>', '|', ':', '"', '.', ','};
            if (ScreenName.IndexOfAny(SpecialCharacters) != -1)
                foreach (char character in SpecialCharacters)
                    ScreenName = ScreenName.Replace(character.ToString(), String.Empty);
            string Path = $"{SubCatalog.FullName}\\{ScreenName}.png";
            ErrorScreen.SaveAsFile(Path, ScreenshotImageFormat.Png);
            Console.WriteLine($"Error screenshot \"{ScreenName}\" saved at {SubCatalog}");
        }

        /// <summary>
        /// Метод ожидания изменения URL
        /// </summary>
        /// <param name="ExpectedUrl">Ожидаемое значение URL (Для успешного завершения метода)</param>
        /// <param name="IterationCount">Количество попыток проверки (Время ожидания каждой - 1 сек.)</param>
        /// <param name="ExceptionText">Текст ошибки в случае неудачи</param>
        public void WaitUntilUrlChanged(string ExpectedUrl, int IterationCount = 10, string ExceptionText = "Exceeded the limit of attempts to check the URL.")
        {
            while (IterationCount != 0)
            {
                System.Threading.Thread.Sleep(1000);
                if (StepsContext.Driver.Url == ExpectedUrl)
                    break;
                IterationCount--;
            }
            if (IterationCount == 0) throw new Exception(ExceptionText);
        }

        

        [Given("I open (.*) widget")]
        public void OpenWidget(string ProductType)
        {
            switch (ProductType.ToUpper())
            {
                case "KASKO": StepsContext.Driver.Url = "https://test-kasko.insapp.ru/"; Element = new LicensePlateElements(); break;
                case "OSAGO": StepsContext.Driver.Url = "https://www.google.com/"; break;
                default: throw new Exception($"Incorrect product type ({ProductType}). Expected \"OSAGO\" or \"KASKO\".");
            }
        }

        [Given("I go on (.*) page")]
        public void GoOnPage(string PageName)
        {
            try
            {
                switch (PageName)
                {
                    case "LicensePlate":
                        if (Element.PageName != "LicensePlate")
                            StepsContext.Driver.Url = "https://test-kasko.insapp.ru/"; break;
                    case "CarData":
                        if (Element.PageName != "CarData")
                        {
                            GoOnPage("LicensePlate");
                            Element.GetWebElement("Number").SendKeys("[581[[");
                            Element.GetWebElement("Region").SendKeys("55");
                            Element.GetWebElement("NextButton").Click();
                            WaitUntilUrlChanged("https://test-kasko.insapp.ru/model", ExceptionText: $"Can't reach the {PageName} page.");
                            Element = new CarDataElements();
                        }
                        break;
                    case "CarParameters":
                        if (Element.PageName != "CarParameters")
                        {
                            GoOnPage("CarData");
                            Element.GetWebElement("Brand").SendKeys("Nissan");
                            Element.GetWebElement("Brand").SendKeys(Keys.Enter);
                            Wait(0.1);
                            Element.GetWebElement("Model").SendKeys("Qashqai");
                            Wait(0.1);
                            Element.GetWebElement("Model").SendKeys(Keys.Enter);
                            Wait(0.1);
                            Element.GetWebElement("NextButton").Click();
                            WaitUntilUrlChanged("https://test-kasko.insapp.ru/info/car", ExceptionText: $"Can't reach the {PageName} page.");
                            Element = new CarParametersElements();
                        }
                        break;
                    case "DriversData":
                        if (Element.PageName != "DriversData")
                        {
                            GoOnPage("CarParameters");
                            Element.GetWebElement("Mileage").SendKeys("100000");
                            Element.GetWebElement("CarPower").SendKeys("144");
                            Element.GetWebElement("ProductionYear").SendKeys("2016");
                            Element.GetWebElement("Price").SendKeys("1200000");
                            Element.GetWebElement("NextButton").Click();
                            WaitUntilUrlChanged("https://test-kasko.insapp.ru/info/drivers", ExceptionText: $"Can't reach the {PageName} page.");
                            Element = new DriversDataElements();
                        }
                        break;
                    case "OwnerData":
                        if (Element.PageName != "OwnerData")
                        {
                            GoOnPage("DriversData");
                            Element.GetWebElement("Age").SendKeys("69");
                            Element.GetWebElement("DriverExperience").SendKeys("21");
                            Element.GetWebElement("MaleGender").Click();
                            Element.GetWebElement("Married").Click();
                            Element.GetWebElement("TwoChild").Click();
                            Element.GetWebElement("NextButton").Click();
                            WaitUntilUrlChanged("https://test-kasko.insapp.ru/info/owner", ExceptionText: $"Can't reach the {PageName} page.");
                            Element = new OwnerDataElements();
                        }
                        break;
                    default:
                        throw new Exception($"Incorrect page name ({PageName}). Expected value from list: LicensePlate," +
                   $" CarData, CarParameters, DriversData, OwnerData");
                }
            }
            catch (Exception ex)
            {
                TakeScreenShot();
                throw ex;
            }

        }

        [When("I enter (.*) in (.*)")]
        public void EnterValueInField(string Value, string Field)
        {
            try
            {
                ClearField(Field);
                ClickOnElement(Field);
                Element.GetWebElement(Field).SendKeys(Value);
            }
            catch (Exception ex)
            {
                TakeScreenShot();
                throw ex;
            }
        }

        [When("With (.*) seconds delay I enter (.*) in (.*)")]
        public void EnterValueInFieldWithDelay(double SecondCount, string Value, string Field)
        {
            try
            {
                ClearField(Field);
                ClickOnElement(Field);
                System.Threading.Thread.Sleep(Convert.ToInt32(SecondCount * 1000));
                foreach (char c in Value)
                {
                    System.Threading.Thread.Sleep(Convert.ToInt32(SecondCount * 1000));
                    Element.GetWebElement(Field).SendKeys(c.ToString());
                }
            }
            catch (Exception ex)
            {
                TakeScreenShot();
                throw ex;
            }
        }

        [When("I clear (.*)")]
        public void ClearField(string Field)
        {
            try
            {
                int FieldLength = Element.GetWebElement(Field).GetAttribute("value").Length;
                while (FieldLength != 0)
                {
                    Element.GetWebElement(Field).SendKeys(Keys.Backspace);
                    FieldLength--;
                }
            }
            catch (Exception ex)
            {
                TakeScreenShot();
                throw ex;
            }
        }

        [When("I press (.*) button")]
        [When("I click on (.*)")]
        public void ClickOnElement(string ClickedElement)
        {
            try
            {
                Element.GetWebElement(ClickedElement).Click();
            }
            catch (Exception ex)
            {
                TakeScreenShot();
                throw ex;
            }
        }

        [When("I press \"(.*)\" key in (.*)")]
        public void PressKey(string Key, string Field)
        {
            try
            {
                switch (Key.ToUpper())
                {
                    case "ENTER": Element.GetWebElement(Field).SendKeys(Keys.Enter); break;
                    case "DOWN": Element.GetWebElement(Field).SendKeys(Keys.ArrowDown); break;
                    case "UP": Element.GetWebElement(Field).SendKeys(Keys.ArrowUp); break;
                    default: throw new Exception($"Incorrect key name ({Key}). Expected Enter, Up or Down");
                }
            }
            catch (Exception ex)
            {
                TakeScreenShot();
                throw ex;
            }
        }

        [When("I wait (.*) seconds")]
        public void Wait(double SecondsCount)
        {
            System.Threading.Thread.Sleep(Convert.ToInt32(SecondsCount * 1000));
        }

        [Then("Attribute (.*) in (.*) should be (.*)")]
        public void CheckValueInField(string Attribute, string Field, string ExpectedValue)
        {
            try
            {
                Element.GetWebElement(Field).GetAttribute(Attribute).Should().Be(ExpectedValue);
            }
            catch (Exception ex)
            {
                TakeScreenShot();
                throw ex;
            }
        }

        [Then("Element (.*) should have (.*) child with (.*) name")]
        public void CheckChildsCount(string ElementName, int ExpectedChildsCount, string ChildsName)
        {
            try
            {
                string ChildsXPath = String.IsNullOrEmpty(ChildsName) ? $".//*" : $".//{ChildsName}";
                Element.GetWebElement(ElementName).FindElements(By.XPath(ChildsXPath)).Count.Should().Be(ExpectedChildsCount);
            }
            catch (Exception ex)
            {
                TakeScreenShot();
                throw ex;
            }
        }
    }
}
