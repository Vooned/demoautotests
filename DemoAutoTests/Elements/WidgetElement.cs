﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using DemoAutoTests.Hooks;
using System.Linq;

namespace DemoAutoTests.Elements
{
    public abstract class WidgetElement
    {
        public string PageName;
        public abstract IWebElement GetWebElement(string ElementName);
    }
}
