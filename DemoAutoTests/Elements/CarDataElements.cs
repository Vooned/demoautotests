﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using DemoAutoTests.Hooks;

namespace DemoAutoTests.Elements
{
    public class CarDataElements: WidgetElement
    {
        public CarDataElements()
        {
            PageName = "CarData";
        }
        /// <summary>
        /// Поле "Марка автомобиля"
        /// </summary>
        private IWebElement Brand
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//*[@fieldname='Марка автомобиля']//input[@role='combobox']")); }
        }

        /// <summary>
        /// Выпадающий список марок автомобиля
        /// </summary>
        private IWebElement BrandsList
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//*[@fieldname='Марка автомобиля']//ng-dropdown-panel[@id='a55a1f52f195']")); }
        }

        /// <summary>
        /// Выбранная марка автомобиля
        /// </summary>
        private IWebElement SelectedBrand
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//*[@fieldname='Марка автомобиля']//span[@class='ng-value-label']")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Марка автомобиля"
        /// </summary>
        private IWebElement BrandError
        {
            get { return StepsContext.Driver.FindElement(By.ClassName("input__error")); }
        }

        /// <summary>
        /// Поле "Модель"
        /// </summary>
        private IWebElement Model
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//*[@fieldname='Модель']//input[@role='combobox']")); }
        }

        /// <summary>
        /// Выпадающий список моделей
        /// </summary>
        private IWebElement ModelsList
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//*[@fieldname='Модель']//ng-dropdown-panel[@id='a55a1f52f195']")); }
        }

        /// <summary>
        /// Выбранная модель
        /// </summary>
        private IWebElement SelectedModel
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//*[@fieldname='Модель']//span[@class='ng-value-label']")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Модель"
        /// </summary>
        private IWebElement ModelError
        {
            get { return StepsContext.Driver.FindElement(By.ClassName("input__error")); }
        }

        /// <summary>
        /// Кнопка "Далее"
        /// </summary>
        public IWebElement NextButton
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//button[@class='button']")); }
        }
        public override IWebElement GetWebElement(string ElementName)
        {
            switch (ElementName)
            {
                case "Brand": return Brand;
                case "BrandsList": return BrandsList;
                case "SelectedBrand": return SelectedBrand;
                case "BrandError": return BrandError;
                case "Model": return Model;
                case "ModelsList": return ModelsList;
                case "SelectedModel": return SelectedModel;
                case "ModelError": return ModelError;
                case "NextButton": return NextButton;
                default: throw new Exception($"Can't find \"{ElementName}\" in {PageName}. Make sure you enter element name correctly.");
            }
        }
    }
}
