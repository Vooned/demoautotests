﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using DemoAutoTests.Hooks;

namespace DemoAutoTests.Elements
{
    class CarParametersElements: WidgetElement
    {
        public CarParametersElements()
        {
            PageName = "CarParameters";
        }

        /// <summary>
        /// Поле "Пробег"
        /// </summary>
        public IWebElement Mileage
        {
            get { return StepsContext.Driver.FindElement(By.Id("mileage")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Пробег"
        /// </summary>
        public IWebElement MileageError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-input-numeric[@fieldid='mileage']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Поле "Мощность"
        /// </summary>
        public IWebElement CarPower
        {
            get { return StepsContext.Driver.FindElement(By.Id("horse-power")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Мощность"
        /// </summary>
        public IWebElement CarPowerError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-input-numeric[@fieldid='horse-power']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Поле "Год выпуска"
        /// </summary>
        public IWebElement ProductionYear
        {
            get { return StepsContext.Driver.FindElement(By.Id("production-year")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Год выпуска"
        /// </summary>
        public IWebElement ProductionYearError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-input-numeric[@fieldid='production-year']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Поле "Текущая стоймость автомобиля"
        /// </summary>
        public IWebElement Price
        {
            get { return StepsContext.Driver.FindElement(By.Id("price")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Текущая стоймость автомобиля"
        /// </summary>
        public IWebElement PriceError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-input-numeric[@fieldid='price']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Кнопка "Назад"
        /// </summary>
        public IWebElement BackButton
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//button[@action='back']")); }
        }

        /// <summary>
        /// Кнопка "Далее"
        /// </summary>
        public IWebElement NextButton
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//button[@tabindex='1']")); }
        }

        public override IWebElement GetWebElement(string ElementName)
        {
            switch (ElementName)
            {
                case "Mileage": return Mileage;
                case "MileageError": return MileageError;
                case "CarPower": return CarPower;
                case "CarPowerError": return CarPowerError;
                case "ProductionYear": return ProductionYear;
                case "ProductionYearError": return ProductionYearError;
                case "Price": return Price;
                case "PriceError": return PriceError;
                case "BackButton": return BackButton;
                case "NextButton": return NextButton;
                default: throw new Exception($"Can't find \"{ElementName}\" in {PageName}. Make sure you enter element name correctly.");
            }
        }
    }
}
