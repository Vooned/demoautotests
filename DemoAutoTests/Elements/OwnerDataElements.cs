﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using DemoAutoTests.Hooks;
using OpenQA.Selenium.Support.UI;

namespace DemoAutoTests.Elements
{
    class OwnerDataElements: WidgetElement
    {
        public OwnerDataElements()
        {
            PageName = "OwnerData";
        }

        /// <summary>
        /// Поле "Город регистрации собственника"
        /// </summary>
        public IWebElement Adress
        {
            get { return StepsContext.Driver.FindElement(By.Id("city")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Город регистрации собственника"
        /// </summary>
        public IWebElement AdressError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-input[@fieldid='city']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Поле "Имя собственника"
        /// </summary>
        public IWebElement OwnerName
        {
            get { return StepsContext.Driver.FindElement(By.Id("username")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Имя собственника"
        /// </summary>
        public IWebElement OwnerNameError
        { get { return StepsContext.Driver.FindElement(By.XPath(".//app-input[@fieldid='username']//span[@class='input__error']")); } }

        /// <summary>
        /// Поле "Телефон собственника"
        /// </summary>
        public IWebElement Phone
        {
            get { return StepsContext.Driver.FindElement(By.Id("phone")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Телефон собственника"
        /// </summary>
        public IWebElement PhoneError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-input[@fieldid='phone']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Сообщение об ошибке в окне "Данные собственника"
        /// </summary>
        public IWebElement OwnerError
        {
            get { return StepsContext.Driver.FindElement(By.ClassName("ModalWindowError")); }
        }

        /// <summary>
        /// Кнопка "Назад"
        /// </summary>
        public IWebElement BackButton
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//button[@tabindex='2']")); }
        }

        /// <summary>
        /// Кнопка "Далее"
        /// </summary>
        public IWebElement NextButton
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//button[@tabindex='1']")); }
        }

        #region Модальное окно ввода смс
        /// <summary>
        /// Модальное окно
        /// </summary>
        public IWebElement SmsWindow
        {
            get { return StepsContext.Driver.FindElement(By.ClassName("sms modal")); }
        }

        /// <summary>
        /// Заголовок модального окна
        /// </summary>
        public IWebElement ModalTitle
        {
            get { return StepsContext.Wait.Until(obj => obj.FindElement(By.ClassName("sms__title"))); }
        }

        /// <summary>
        /// Сообщение об ошибке в модальном окне
        /// </summary>
        public IWebElement ModalWindowError
        {
            get { return StepsContext.Driver.FindElement(By.ClassName("error-block")); }
        }

        /// <summary>
        /// Описание модального окна
        /// </summary>
        public IWebElement Description
        {
            get { return StepsContext.Driver.FindElement(By.ClassName("sms__text")); }
        }

        /// <summary>
        /// Инпуты для ввода смс
        /// </summary>
        /// <param name="InputNumber"></param>
        /// <returns></returns>
        public IWebElement SmsInput(int InputNumber)
        {
            switch (InputNumber)
            {
                case 1: return StepsContext.Wait.Until(obj => obj.FindElement(By.Id("code0")));
                case 2: return StepsContext.Wait.Until(obj => obj.FindElement(By.Id("code1")));
                case 3: return StepsContext.Wait.Until(obj => obj.FindElement(By.Id("code2")));
                case 4: return StepsContext.Wait.Until(obj => obj.FindElement(By.Id("code3")));
                default:
                    throw new Exception($"Incorrect \"InputNumber\" ({InputNumber}) in {PageName}. Expected value from list: " +
               $"1 (First input), 2 (Second input), 3 (Third input), 4 (Fourth input)");
            }
        }

        /// <summary>
        /// Кнопка "Отправить смс ещё раз"
        /// </summary>
        public IWebElement ResendSmsButton
        {
            get { return StepsContext.Driver.FindElement(By.ClassName("sms__resend")); }
        }

        /// <summary>
        /// Описание пользовательского соглашения
        /// </summary>
        public IWebElement AgreementDescription
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//section[@class='sms modal']//p[@class='sms__text sms__text--agreement']")); }
        }

        /// <summary>
        /// Кнопка скачивания пользовательского соглашения
        /// </summary>
        public IWebElement DownloadAgreementButton
        {
            get { return StepsContext.Driver.FindElement(By.ClassName("sms__link")); }
        }
        #endregion

        public override IWebElement GetWebElement(string ElementName)
        {
            switch (ElementName)
            {
                case "Adress": return Adress;
                case "AdressError": return AdressError;
                case "OwnerName": return OwnerName;
                case "OwnerNameError": return OwnerNameError;
                case "Phone": return Phone;
                case "PhoneError": return PhoneError;
                case "OwnerError": return OwnerError;
                case "BackButton": return BackButton;
                case "NextButton": return NextButton;
                case "SmsWindow": return SmsWindow;
                case "ModalTitle": return ModalTitle;
                case "ModalWindowError": return ModalWindowError;
                case "Description": return Description;
                case "FirstSmsInput": return SmsInput(1);
                case "SecondSmsInput": return SmsInput(2);
                case "ThirdSmsInput": return SmsInput(3);
                case "FourthSmsInput": return SmsInput(4);
                case "ResendSmsButton": return ResendSmsButton;
                case "AgreementDescription": return AgreementDescription;
                case "DownloadAgreementButton": return DownloadAgreementButton;
                default: throw new Exception($"Can't find \"{ElementName}\" in {PageName}. Make sure you enter element name correctly.");
            }
        }
    }
}
