﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using DemoAutoTests.Hooks;

namespace DemoAutoTests.Elements
{
    class DriversDataElements: WidgetElement
    {
        public DriversDataElements()
        {
            PageName = "DriversData";
        }

        /// <summary>
        /// Контейнер с кнопками водителей
        /// </summary>
        public IWebElement DriversCount
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//ol[@class='drivers__tabs tabs']")); }
        }

        /// <summary>
        /// Поле "Возраст водителя"
        /// </summary>
        public IWebElement Age
        {
            get { return StepsContext.Driver.FindElement(By.Id("age")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Возраст водителя"
        /// </summary>
        public IWebElement AgeError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-input-numeric[@fieldid='age']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Поле "Водительский стаж"
        /// </summary>
        public IWebElement DriverExperience
        {
            get { return StepsContext.Driver.FindElement(By.Id("experience")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Водительский стаж"
        /// </summary>
        public IWebElement DriverExperienceError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-input-numeric[@fieldid='experience']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Поле "Пол водителя"
        /// </summary>
        public IWebElement Gender
        {
            get { return StepsContext.Driver.FindElement(By.Id("gender")); }
        }

        /// <summary>
        /// Значения поля "Пол водителя"
        /// </summary>
        /// <param name="GenderNumber"></param>
        /// <returns></returns>
        public IWebElement GenderOptions(int GenderNumber)
        {
            switch (GenderNumber)
            {
                case 1: return StepsContext.Driver.FindElement(By.XPath(".//select[@id='gender']//option[@value=1]"));
                case 2: return StepsContext.Driver.FindElement(By.XPath(".//select[@id='gender']//option[@value=2]"));
                default: throw new Exception($"Incorrect \"GenderNumber\" ({GenderNumber}) in {PageName}. Expected 1 (Male) or 2 (Female).");
            }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Пол водителя"
        /// </summary>
        public IWebElement GenderError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-select[@fieldid='gender']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Поле "Семейный статус"
        /// </summary>
        public IWebElement MaritalStatus
        {
            get { return StepsContext.Driver.FindElement(By.Id("familyStatus")); }
        }

        /// <summary>
        /// Значения поля "Семейный статус"
        /// </summary>
        /// <param name="MaritalNumber"></param>
        /// <returns></returns>
        public IWebElement MaritalStatusOptions(int MaritalNumber)
        {
            switch (MaritalNumber)
            {
                case 1: return StepsContext.Driver.FindElement(By.XPath(".//select[@id='familyStatus']//option[@value=1]"));
                case 2: return StepsContext.Driver.FindElement(By.XPath(".//select[@id='familyStatus']//option[@value=2]"));
                default: throw new Exception($"Incorrect \"MaritalNumber\" ({MaritalNumber}) in {PageName}. Expected 1 (Unmarried) or 2 (Married).");
            }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Семейный статус"
        /// </summary>
        public IWebElement MaritalStatusError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-select[@fieldid='familyStatus']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Поле "Количество детей"
        /// </summary>
        public IWebElement ChildCount
        {
            get { return StepsContext.Driver.FindElement(By.Id("children")); }
        }

        /// <summary>
        /// Значения поля "Количество детей"
        /// </summary>
        /// <param name="ChildNumber"></param>
        /// <returns></returns>
        public IWebElement ChildCountOptions(int ChildNumber)
        {
            switch (ChildNumber)
            {
                case 0: return StepsContext.Driver.FindElement(By.XPath(".//select[@id='children']//option[@value=0]"));
                case 1: return StepsContext.Driver.FindElement(By.XPath(".//select[@id='children']//option[@value=1]"));
                case 2: return StepsContext.Driver.FindElement(By.XPath(".//select[@id='children']//option[@value=2]"));
                case 3: return StepsContext.Driver.FindElement(By.XPath(".//select[@id='children']//option[@value=3]"));
                case 4: return StepsContext.Driver.FindElement(By.XPath(".//select[@id='children']//option[@value=4]"));
                case 5: return StepsContext.Driver.FindElement(By.XPath(".//select[@id='children']//option[@value=5]"));
                default: throw new Exception($"Incorrect \"ChildNumber\" ({ChildNumber}) in {PageName}. " +
                    $"Expected value from list: 0 (No children), 1 (1 children), 2 (2 childrens), 3 (3 childrens), 4 (4 childrens), 5 (5 or more childrens).");
            }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Количество детей"
        /// </summary>
        public IWebElement ChildCountError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-select[@fieldid='children']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Кнопка "Добавить водителя"
        /// </summary>
        public IWebElement AddDriverButton
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//button[@tabindex='3']")); }
        }

        /// <summary>
        /// Кнопка "Назад"
        /// </summary>
        public IWebElement BackButton
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//button[@tabindex='2']")); }
        }

        /// <summary>
        /// Кнопка "Все водители добавлены"
        /// </summary>
        public IWebElement NextButton
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//button[@tabindex='1']")); }
        }

        public override IWebElement GetWebElement(string ElementName)
        {
            switch (ElementName)
            {
                case "DriversCount": return DriversCount;
                case "Age": return Age;
                case "AgeError": return AgeError;
                case "DriverExperience": return DriverExperience;
                case "DriverExperienceError": return DriverExperienceError;
                case "Gender": return Gender;
                case "GenderError": return GenderError;
                case "MaleGender": return GenderOptions(1);
                case "FemaleGender": return GenderOptions(2);
                case "MaritalStatus": return MaritalStatus;
                case "MaritalStatusError": return MaritalStatusError;
                case "Unmarried": return MaritalStatusOptions(1);
                case "Married": return MaritalStatusOptions(2);
                case "ChildCount": return ChildCount;
                case "ChildCountError": return ChildCountError;
                case "NoChild": return ChildCountOptions(0);
                case "OneChild": return ChildCountOptions(1);
                case "TwoChild": return ChildCountOptions(2);
                case "ThreeChild": return ChildCountOptions(3);
                case "FourChild": return ChildCountOptions(4);
                case "FiveOrMoreChild": return ChildCountOptions(5);
                case "AddDriverButton": return AddDriverButton;
                case "BackButton": return BackButton;
                case "NextButton": return NextButton;
                default: throw new Exception($"Can't find \"{ElementName}\" in {PageName}. Make sure you enter element name correctly.");
            }
        }
    }
}
