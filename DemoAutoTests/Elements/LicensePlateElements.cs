﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using DemoAutoTests.Hooks;

namespace DemoAutoTests.Elements
{
    class LicensePlateElements: WidgetElement
    {
        public LicensePlateElements()
        {
            PageName = "LicensePlate";
        }

        /// <summary>
        /// Поле "Госномер автомобиля"
        /// </summary>
        public IWebElement Number
        {
            get { return StepsContext.Driver.FindElement(By.Id("number")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Госномер автомобиля"
        /// </summary>
        public IWebElement NumberError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-input[@fieldid='number']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Поле "Код региона"
        /// </summary>
        public IWebElement Region
        {
            get { return StepsContext.Driver.FindElement(By.Id("region")); }
        }

        /// <summary>
        /// Сообщение об ошибке в поле "Код региона"
        /// </summary>
        public IWebElement RegionError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//app-input[@fieldid='region']//span[@class='input__error']")); }
        }

        /// <summary>
        /// Общее сообщение об ошибке экрана "Номер автомобиля"
        /// </summary>
        public IWebElement LicensePlateError
        {
            get { return StepsContext.Driver.FindElement(By.XPath(".//form[@id='licenseForm']//p[@class='license__error']")); }
        }

        /// <summary>
        /// Кнопка "Далее"
        /// </summary>
        public IWebElement NextButton
        {
            get { return StepsContext.Driver.FindElement(By.ClassName("button")); }
        }

        public override IWebElement GetWebElement(string ElementName)
        {
            switch (ElementName)
            {
                case "Number": return Number;
                case "NumberError": return NumberError;
                case "Region": return Region;
                case "LicensePlateError": return LicensePlateError;
                case "RegionError": return RegionError;
                case "NextButton": return NextButton;
                default: throw new Exception($"Can't find \"{ElementName}\" in {PageName}. Make sure you enter element name correctly.");
            }
        }
    }
}
